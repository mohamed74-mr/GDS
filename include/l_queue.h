#ifndef QUEUE_H
#define QUEUE_H

#include "../include/l_list.h"

typedef struct queue_t {
	/* int front; */
	size_t rear;
	List *data;
} Queue;

Queue*    Q_Init();
size_t    Q_Enqueue(Queue *q, void *item);
void*     Q_Dequeue(Queue *q);
void*     Q_Front(Queue *q);
void*     Q_Rear(Queue *q);
size_t    Q_Size(Queue *);
int       Q_Is_Empty(Queue *q);
/* void*     Q_Traverse(Queue *q); */
int       Q_Delete(Queue *q);
Queue*    Q_Clear(Queue *q);


#endif /* QUEUE_H */
