#ifndef STACK_H
#define STACK_H

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "l_list.h"

typedef struct stack_t {
	size_t top;
	List *data;	
} Stack;

Stack* Stack_Init();
int    Stack_Push(Stack *stack, void *item);
void*  Stack_Pop(Stack *stack);
void*  Stack_Top(Stack *stack);
size_t Stack_Size(Stack *stack);
/* void*  Stack_Traverse(Stack *stack); */
int    Stack_Delete(Stack *stack);

#endif /*STACK_H */
