#ifndef L_LIST_H
#define L_LIST_H

#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct _node {
	void *data;
	struct _node *next;
	struct _node *prev;
} Node;

typedef struct _list {
	struct _node *head;
	struct _node *tail;
	size_t len;
} List;


/* *******************************
 * index used to traverse the list  
 *********************************/
extern size_t _current_index;
/* size_t _current_index = 0; */

List*    List_Init();
size_t   List_Len(List *list);
int      List_Insert_First(List *list, void *data);
int      List_Insert_Last(List *list, void *data);
int      List_Insert_At(List *list, void *data, size_t index);
void*    List_Remove_At(List *list, size_t index);
void*    List_Remove_First(List *list);
void*    List_Remove_Last(List *list);
void*    List_Get_First(List *list);
void*    List_Get_Last(List *list);
void*    List_Get_At(List* list, size_t index);
int      List_Is_Empty(List *list);
/* void*    List_Traverse(List *list); */
void     List_Sort(List *list);
List*    List_Clear(List *list);
int      List_Delete(List *list);
/* void     List_Set_Current_Index(size_t *_index); */




#endif /* L_LIST_H */
