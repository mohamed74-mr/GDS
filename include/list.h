#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>


#define DECL_LIST(typename, type)								\
	typedef struct type##_node {								\
		type data;												\
		struct type##_node *next, *prev;						\
	} type##_node;												\
																\
	typedef struct typename {									\
		size_t len;												\
		type##_node *head;										\
		type##_node *last;										\
	} typename;													\
																\
	typename* typename##_create()								\
	{															\
		typename *list = malloc(sizeof(typename));				\
		assert(list);											\
		list->head = malloc(sizeof(type##_node));				\
		assert(list->head);										\
		list->last = malloc(sizeof(type##_node));				\
		assert(list->last);										\
		list->head->next = list->last;							\
		list->last->prev = list->head;							\
		list->last->next = NULL;								\
		list->len = 0;											\
		return list;											\
	}															\
																\
	size_t typename##_append(typename *list, type data)			\
	{															\
		if (list->len == 0) {									\
			list->head->data = data;							\
			list->head = list->last;							\
			list->last->next = NULL;							\
		} else {												\
			type##_node *node = malloc(sizeof(type##_node));	\
			assert(node);										\
			node->data = data;									\
			node->prev = list->last->prev;						\
			node->next = list->last;							\
			list->last->next = node;							\
			node = list->last;									\
			list->last = list->last->next;						\
			list->last->next =	NULL;							\
		}														\
		list->len++;											\
		return list->len;										\
	}															\
																\
	size_t typename##_insert_first(typename *list, type data)	\
	{															\
		assert(list);											\
		if (list->len == 0) {									\
			list->head->data = data;							\
			list->head->next = list->last;						\
			list->last->next = NULL;							\
		} else { 												\
			type##_node *node = malloc(sizeof(type##_node));	\
			assert(node);										\
			node->data = data;									\
			node->next =list->head;								\
			list->head = node;									\
		}														\
		list->len++;											\
		return list->len;										\
	}															\
																\
	int typename##_delete(typename *list)						\
	{															\
		if (list) {												\
			type##_node *node = malloc(sizeof(type##_node));	\
			node  = list->head;									\
			while (node->next != NULL) {						\
				free(node);										\
				node = node->next;								\
			}													\
			free(node);											\
			free(list->last);									\
			list = NULL;										\
			free(list);											\
		}														\
		if (list) {												\
			return -1;											\
		}														\
		return 0;												\
	}															\
																\
	bool typename##_isempty(typename *list)						\
	{															\
		assert(list);											\
		if (list->len == 0) {									\
			return true;										\
		} else {												\
			return true;										\
		}														\
	}															\
																\
	type typename##_remove_last(typename *list)					\
	{															\
		if (typename##_isempty(list)) {							\
			printf("list is empty!!\n");						\
			exit(EXIT_FAILURE);									\
		}														\
		type##_node *node = list->last;							\
		list->last = list->last->prev;							\
		list->last->next = NULL;								\
		return node->data;										\
	}															\
																\
	type typename##_remove(typename *list, type item)			\
	{															\
		type##_node *node = malloc(sizeof(type##_node));		\
		type data;												\
		assert(node);											\
		node = list->head;										\
		while (node != NULL) {									\
			if (node->data == item) {							\
				data = node->data;								\
				free(node);										\
				return data;									\
			}													\
			node = node->next;									\
		}														\
		free(node);												\
		printf("item not found in the list\n");					\
		exit(EXIT_FAILURE);										\
	}															\
																\
	int typename##_remove_at(typename *list, size_t index)		\
	{															\
		assert(list);											\
		type##_node *current = malloc(sizeof(type##_node));		\
		type##_node *temp = malloc(sizeof(type##_node));		\
		assert(current);										\
		assert(temp);											\
		current = list->head;									\
		if (index > list->len) {								\
			return -1;											\
		}														\
		for (size_t i = 0; i < index-1; i++) {					\
			current = current->next;							\
		}														\
		temp = current->next;									\
		free(temp);												\
		current->next = current->next->next;					\
		free(current);											\
		return 1;												\
	}															\


#endif /* LIST_H */
