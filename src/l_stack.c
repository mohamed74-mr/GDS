#include "../include/l_stack.h"

Stack*
Stack_Init()
{
	Stack *stack = (Stack *)malloc(sizeof(Stack));
	stack->data = List_Init();
	assert(stack->data);
	stack->top = 0;
	return stack;
}

int
Stack_Push(Stack *stack, void *item)
{
	assert(stack);
	List_Insert_At(stack->data, item, stack->top);
	stack->top++;
	return stack->top;
}

void*
Stack_Pop(Stack *stack)
{
	assert(stack);
	stack->top--;
	return (List_Remove_Last(stack->data));
}

void*
Stack_Top(Stack *stack)
{
	assert(stack);
	return (List_Get_Last(stack->data));
}

size_t
Stack_Size(Stack *stack)
{
	assert(stack);
	return stack->top;
}

void*
Stack_Traverse(Stack *stack)
{
	static size_t index = 0;
	assert(stack);
	return (List_Get_At(stack->data, index));
}

int
Stack_Delete(Stack *stack)
{
	assert(stack);
	if (List_Delete(stack->data) != 0) {
		stack->top = 0;
		return 1;
	}	
	return 0;
}
