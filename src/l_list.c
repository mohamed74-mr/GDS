#include "../include/l_list.h"

List*
List_Init()
{
	List *list = (List *) malloc(sizeof(List));
	assert(list);
	list->head = (Node *)malloc(sizeof(Node));
	list->tail = (Node *)malloc(sizeof(Node));
	assert(list->head && list->tail);
	list->tail = list->head;
	list->tail->next = NULL;
	list->len = 0;
	return list;
}

int
List_Delete(List *list)
{
	if (list) {
		/* assert(list->head); */
		if (list->head) {
			Node *node = (Node *)malloc(sizeof(Node));
			assert(node);
			node = list->head;
			while (list->head != NULL) {				
				free((void *)node);
				list->head = list->head->next;
				node = list->head;
			}
			free((void *)list->tail);
			free((void *)list->head);
			free((void *)node);
			free((void *)list);
		} else {
			free((void *)list->head);
			free((void *)list->tail);
			free((void *)list);
		}		
	}
	if (list) {
		return 1;
	} else {
		return 0;
	}
}

size_t
List_Len(List *list)
{
	assert(list);
	return list->len;
}

int
List_Insert_First(List *list, void *data)
{
	assert(list);	
	if (list->len == 0) {
		if (!list->head) {
			list->head = (Node *)malloc(sizeof(Node));
			list->tail = (Node *)malloc(sizeof(Node));
		}
		list->head->data = malloc(sizeof(data));
		memcpy(list->head->data, data, sizeof(data));		
		list->tail = list->head;
	} else {
		Node *node = (Node *)malloc(sizeof(Node));
		assert(node);
		node->data = malloc(sizeof(data));
		memcpy(node->data, data, sizeof(data));
		list->head->prev = node;
		node->next = list->head;
		list->head = node;
	}												
	list->len++;
	return list->len;
}

int List_Insert_Last(List *list, void *data)
{
	assert(list);
	if (list->len == 0) {
		if (!list->head) {
			list->head = (Node *)malloc(sizeof(Node));
			list->tail = (Node *)malloc(sizeof(Node));
		}		
		list->head->data = malloc(sizeof(data));
		memcpy(list->head->data, data, sizeof(data));		
		list->tail = list->head;		
		list->tail->next = NULL;				
	} else {
		Node *node = (Node *)malloc(sizeof(Node));	
		assert(node);
		node->data = malloc(sizeof(data));
		assert(data);
		node->data = memcpy(node->data, data, sizeof(data));
		node->prev = list->tail;
		list->tail->next = node;
		list->tail = node;
		list->tail->next = NULL;
	}
	list->len++;
	return list->len;
}

int
List_Insert_At(List *list, void *data, size_t index)
{
	assert(list);
	if (index < 0) {
		return -1;
	} else if (index == 0) {
		List_Insert_First(list, data);
	} else if (index >= list->len) {
		List_Insert_Last(list, data);
	} else {
		size_t count = 0;
		Node *temp = (Node *)malloc(sizeof(Node));
		assert(temp);
		temp = list->head;
		for (count = 0; count < index-1; count++) {
			temp = temp->next;
		}
		Node *node = (Node *)malloc(sizeof(Node));
		assert(node);
		node->data = malloc(sizeof(data));
		memcpy(node->data, data, sizeof(data));
		node->prev = temp;
		node->next = temp->next;		
		list->len++;
		free(temp);
	}
	return list->len;
}

void*
List_Get_First(List *list)
{
	assert(list);
	void *data = list->head->data;
	return data;
}

void*
List_Get_Last(List *list)
{
	assert(list);
	assert(list->tail);
	void *data = list->tail->data;
	return (data);
}

void*
List_Get_At(List *list, size_t index)
{
	assert(list);
	assert(index >= 0);	
	if (index == 0) {
		return (List_Get_First(list));
	} else if (index == list->len-1) {
		return (List_Get_Last(list));
	} else {
		size_t count = 0;
		void *data;
		Node *node = (Node *)malloc(sizeof(Node));
		assert(node);
		node = list->head;
		for (count = 0; count < index && node->next != NULL; count++) {
			node = node->next;
		}
		data = node->data;
		free((void *)node);
		return data;
	}		
}

void*
List_Remove_First(List *list)
{	
	assert(list);
	assert(list->len > 0);
	assert(list->head);
	void *data = malloc(sizeof(list->head->data));
	Node *temp = (Node *)malloc(sizeof(Node));
	assert(temp);
	temp = list->head;
	data = memcpy(data, list->head->data, sizeof(list->head->data));
	free(temp);
	list->head = (Node *)malloc(sizeof(Node));
	/* assert(list->head); */
	list->head = list->head->next;
	list->len--;
	/* free(temp);	 */
	return data;
}

void* List_Remove_Last(List *list)
{
	assert(list);
	assert(list->len > 0);
	if (list->len == 1) {
		return List_Remove_First(list);
	} 
	void *data = malloc(sizeof(list->tail->data));
	data = memcpy(data, list->tail->data, sizeof(list->tail->data));
	Node *temp = (Node *)malloc(sizeof(Node));
	assert(temp);
	temp = list->tail;
	free(temp);
	list->tail->next = NULL;
	list->tail = list->tail->prev;
	list->len--;
	return data;
}

void*
List_Remove_At(List *list, size_t index)
{
	assert(list);
	assert(list->len > 0);
	assert(index >= 0 && index < list->len);	
	if (index == 0) {
		return List_Remove_First(list);
	} else if (index == list->len-1) {
		return List_Remove_Last(list);
	} else {
		void *data = malloc(sizeof(list->tail->data));
		Node *temp = (Node *)malloc(sizeof(Node));
		Node *current = (Node *)malloc(sizeof(Node));
		assert(temp && current);
		current = list->head;
		for (size_t i = 0; i < index && current->next != NULL; i++) {
			current = current->next;
		}
		data = memcpy(data, current->data, sizeof(current->data));
		temp = current;
		free(temp);
		current = (Node *)malloc(sizeof(Node));
		current = current->next;
		/* current->next = temp->next; */
		/* free(temp); */
		list->len--;
		return data;
	}	
}

int
List_Is_Empty(List *list)
{
	if (list->len == 0) {
		return 1;
	}
	return (list->len);
}

void
List_Sort(List *list)
{
	
}

List*
List_Clear(List *list)
{
	List_Delete(list);
	list = List_Init();
	return list;
}
