#include <stdio.h>


/* #include "l_list.h" */
/* #include "list.h" */

/* DECL_LIST(int_list, int); */
/* DECL_LIST(char_list, char); */

/* int main() */
/* { */
	
/* 	int_list *list1 = int_list_create(); */
/* 	/\* char_list *list2 = char_list_create(); *\/ */
	
/* 	for (int i = 0; i < 10; i++) { */
/* 		int_list_append(list1, i); */
/* 		/\* char_list_append(list2, 'a'+i); *\/ */
/* 		printf("%ld- ", list1->len); */
/* 		printf("%d\n", list1->last->data); */
/* 	}		 */
/* 	printf("\n"); */
/* 	int_node *node = malloc(sizeof(int_node)); */

/* 	node = list1->head; */
/* 	while (node != NULL) { */
/* 		printf("%d ", node->data); */
/* 		node = node->next; */
/* 	} */
/* 	printf("\n"); */

/* 	/\* if (int_list_delete(list1) == 0) { *\/ */
/* 	/\* 	printf("list is successfully deleted\n"); *\/ */
/* 	/\* } else { *\/ */
/* 	/\* 	printf("error : couldn't delete the list\n"); *\/ */
/* 	/\* } *\/ */

/* 	/\* for (size_t i = list1->len; i >= 0; i--) { *\/ */
/* 	/\* 	/\\* printf("%d ", int_list_remove_last(list1)); *\\/ *\/ */
/* 	/\* } *\/ */
/* 	/\* printf("\n"); *\/ */
	
/* 	free(node); */

/* /\* node = int_list_remove(list1); *\/ */
/* 	/\* printf("%d ", node->data); *\/ */
/* 	/\* node = int_list_remove(list1); *\/ */
/* 	/\* printf("%d ", node->data); *\/ */
/* 	/\* node = int_list_remove(list1); *\/ */
/* 	/\* printf("%d ", node->data); *\/ */
/* 	/\* printf("\n"); *\/ */
/* 	/\* printf("%d ", list1->last->data); *\/ */
/* 	/\* printf("\n"); *\/ */
	
/* /\* for (size_t i = 0; i  < list1->len; i++) { *\/ */
/* 	/\* 	node = int_list_remove(list1); *\/ */
/* 	/\* 	assert(node); *\/ */
/* 	/\* 	printf("%d ", node->data); *\/ */
/* 	/\* } *\/ */
/* 	/\* printf("\n"); *\/ */

/* 	/\* free(node); *\/ */
/* 	/\* printf("%ld\n", list2->len); *\/ */
/* 	/\* printf("%c\n", list2->last->data); *\/ */

/* 	/\* int_list_print(list1); *\/ */
	
	
/* 	return 0; */
/* } */


/* ********************************** */
/* linked list testing  ************* */
/* ********************************** */
/* #include "../include/l_list.h" */

/* int main(int argc, char **argv) */
/* { */
/* 	List *list; */
/* 	list = List_Init(); */
		
/* 	int i = 0; */
/* 	for (i = 0; i < 10; i++) { */
/* 		List_Insert_First(list, &i); */
/* 		/\* printf("%d, ", *(int *)list->tail->data); *\/ */
/* 	} */
/* 	printf("\n"); */
/* 	int j = 20; */
/* 	List_Insert_Last(list, &j); */
/* 	j = 70; */
/* 	List_Insert_Last(list, &j); */
/* 	j = 80; */
/* 	List_Insert_Last(list, &j); */
	
/* 	/\* for (size_t i = 0; i < List_Len(list); i++) { *\/ */
/* 	/\* 	printf("%d, ", *(int *)List_Traverse(list)); *\/ */
/* 	/\* } *\/ */
/* 	printf("\n"); */
/* 	printf("list length: %ld \n", List_Len(list)); */
/* 	while (List_Len(list) > 0) { */
/* 		printf("%d, ", *(int *)List_Remove_Last(list)); */
/* 	} */
/* 	printf("\n"); */


/* 	i = 0; */
/* 	for (i = 0; i < 10; i++) { */
/* 		List_Insert_First(list, &i); */
/* 		/\* printf("%d, ", *(int *)list->tail->data); *\/ */
/* 	} */
/* 	printf("\n"); */
/* 	j = 20; */
/* 	List_Insert_Last(list, &j); */
/* 	j = 70; */
/* 	List_Insert_Last(list, &j); */
/* 	j = 80; */
/* 	List_Insert_Last(list, &j); */
	
/* 	/\* for (size_t i = 0; i < List_Len(list); i++) { *\/ */
/* 	/\* 	printf("%d, ", *(int *)List_Traverse(list)); *\/ */
/* 	/\* } *\/ */
/* 	printf("\n"); */
/* 	printf("list length: %ld \n", List_Len(list)); */
/* 	while (List_Len(list) > 0) { */
/* 		printf("%d, ", *(int *)List_Remove_Last(list)); */
/* 	} */
/* 	printf("\n"); */

/* 	for (int k = 0; k < 20; k++) { */
/* 		List_Insert_First(list, &k); */
/* 	} */
	
/* 	for (int i = 0; i < List_Len(list); i++) { */
/* 		printf("%d, ", *(int *)List_Remove_At(list, i)); */
/* 	} */
/* 	printf("\n"); */
/* 	List_Delete(list); */
/* 	return 0; */
/* } */



/* ********************************** */
/* stack testing ******************** */
/* ********************************** */
/* #include "../include/l_stack.h" */

/* int main(int argc, char **argv) */
/* { */
   
/* 	Stack *stack = Stack_Init(); */
/* 	for (int i = 0; i < 10; i++) { */
/* 		Stack_Push(stack, &i); */
/* 	} */
	
/* 	/\* for (int i = 0; i < 10; i++) { *\/ */
/* 	/\* 	printf("%d, ", *(int *)Stack_Traverse(stack)); *\/ */
/* 	/\* } *\/ */
/* 	/\* printf("\n"); *\/ */
/* 	printf("stack size: %ld \n", Stack_Size(stack)); */
	
/* 	for (int i = 0; i < 10; i++) { */
/* 		printf("%d, ", *(int *)Stack_Pop(stack)); */
/* 	} */
/* 	printf("\n"); */
/* 	printf("stack size: %ld \n", Stack_Size(stack)); */
	
/* 	for (int i = 0; i < 10; i++) { */
/* 		Stack_Push(stack, &i); */
/* 	} */
			
/* 	printf("\n"); */
/* 	for (int i = 0; i < 10; i++) { */
/* 		printf("%d, ", *(int *)Stack_Pop(stack)); */
/* 	} */
/* 	printf("\n"); */
/* 	printf("stack size: %ld \n", Stack_Size(stack)); */
/* 	int j = 74; */
/* 	Stack_Push(stack, &j); */
/* 	j = 99; */
/* 	Stack_Push(stack, &j); */
/* 	j = 25; */
/* 	Stack_Push(stack, &j); */
/* 	printf("\n"); */
/* 	for (int i = Stack_Size(stack); i > 0 ; i--) { */
/* 		printf("%d, ", *(int *)Stack_Pop(stack)); */
/* 	} */
/* 	/\* while (Stack_Size(stack) > 0) { *\/ */
/* 		/\* printf("%d, ", *(int *)Stack_Pop(stack)); *\/ */
/* 	/\* } *\/ */
/* 	printf("\n"); */
/* 	printf("stack size: %ld \n", Stack_Size(stack)); */
	   
/* 	return 0; */
/* } */


/* ************************ */
/* testing queue ********** */
/* ************************ */
#include "../include/l_queue.h"

int
main(int argc, char **argv)
{
	size_t i = 0;
	Queue *q = Q_Init();
	for (i = 0; i < 10; i++) {
		Q_Enqueue(q, &i);
	}

	/* for (int i = 0; i < 10; i++) { */
	/* 	printf("%d, ", *(int *)Q_Traverse(q)); */
	/* } */
	/* printf("\n"); */
	printf("queue size: %ld \n", Q_Size(q));
	
	for (i = Q_Size(q); i > 0; i--) {
		printf("%d, ", *(int *)Q_Dequeue(q));
	}
	printf("\n");
	printf("queue size: %ld \n", Q_Size(q));
	int j = 10;
	Q_Enqueue(q, &j);
	j = 74;
	Q_Enqueue(q, &j);
	for (i = 0; i < 10; i++) {
		Q_Enqueue(q, &i);
	}

	/* for (int i = 0; i < 10; i++) { */
	/* 	printf("%d, ", *(int *)Q_Traverse(q)); */
	/* } */
	while (Q_Size(q) > 0) {
		printf("%d, ", *(int *)Q_Dequeue(q));
	}
	printf("\n");
	printf("queue size: %ld \n", Q_Size(q));
	
	/* for (int i = 0; i < 10; i++) { */
	/* 	printf("%d, ", *(int *)Q_Dequeue(q)); */
	/* } */
	/* printf("\n"); */
	/* printf("queue size: %ld \n", Q_Size(q)); */
	/* j = 10; */
	/* Q_Enqueue(q, &j); */
	/* j = 74; */
	/* Q_Enqueue(q, &j); */
		
	return 0;
}
