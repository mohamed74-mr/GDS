#include "../include/l_queue.h"

extern size_t _current_index;

Queue*
Q_Init()
{
	Queue *q = (Queue *)malloc(sizeof(Queue));
	assert(q);
	q->data = List_Init();
	List_Init(q->data);
	assert(q->data);
	q->rear = 0;
	return q;
}

size_t
Q_Enqueue(Queue *q, void *item)
{
	assert(q);
	
	if (q->rear == 0) {		
		q->data = List_Init();
	}
	List_Insert_Last(q->data, item);
	q->rear = List_Len(q->data);
	return (q->rear);
}

void*
Q_Dequeue(Queue *q)
{
	assert(q);
	assert(q->rear > 0);
	q->rear--;
	return (List_Remove_First(q->data));	
}

void*
Q_Rear(Queue *q)
{
	assert(q);
	return (List_Get_Last(q->data));
}

void*
Q_Front(Queue *q)
{
	assert(q);
	return (List_Get_First(q->data));
}

size_t
Q_Size(Queue *q)
{
	assert(q);
	return (List_Len(q->data));
	/* return q->rear - 1; */
}

int
Q_Is_Empty(Queue *q)
{
	assert(q);
	return (List_Is_Empty(q->data));
}

/* void* */
/* Q_Traverse(Queue *q) */
/* { */
/* 	/\* if (Q_Size(q) < 1) { *\/ */
/* 	/\* 	List_Set_Current_Index(0); *\/ */
/* 	/\* }	 *\/ */
/* 	return (List_Traverse(q->data)); */
/* } */

int
Queue_Delete(Queue *q)
{
	assert(q);
	if (List_Delete(q->data)) {
		q->rear = 0;
		free(q);
		return 1;
	}
	return 0;
}

Queue*
Q_Clear(Queue *q)
{
	assert(q);
	List_Clear(q->data);
	q = Q_Init();
	return q;
}
